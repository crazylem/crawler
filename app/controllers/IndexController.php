<?php

namespace App\Controllers;

use App\Components\Parser;

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        $parser = new Parser();
        $this->view->setVar('result', $parser->handle());
    }
}

