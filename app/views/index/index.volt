<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <label for="searchstring">Искать:</label>
            {{ text_field('searchstring') }}
        </div>
        <div class="col-sm-8">
            {% for cur_row in result %}
                {{ cur_row }}
            {% endfor %}
        </div>
    </div>
</div>
